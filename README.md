# pdf_merge



## Install

```
$ pip install PyPDF2
```

## Usage

Create a directory and insert your pdf files.

```
$ python main.py
```

## Folder structure
```
.
├── merge_dir
│   ├── 001.pdf (Named ascending)
│   ├── 002.pdf
│   └── 003.pdf
├── main.py
├── ./merge_dir.pdf (Success merge file)
└── README.md
```
