from PyPDF2 import PdfFileMerger
import os

path = './'
dirs = os.listdir(path)
series = []
for dir in dirs:
    # If you need to merge certain directory
    if os.path.isdir(os.path.join(path, dir)) and dir=='merge_dir':
        series.append(dir)

    # Merge all directory under root
    # if os.path.isdir(os.path.join(path, dir)):
    #     series.append(dir)
series.sort()
print(series)
for dir in series:
    # List all files(*.pdf) and sort by their name.
    pdf_files = os.listdir(os.path.join(path,dir))
    pdf_files.sort()
    #Create and instance of PdfFileMerger() class
    merger = PdfFileMerger()

    for pdf_file in pdf_files:
        if(pdf_file != '.DS_Store'):
        # Make sure all you input is pdf file type.
        # Append PDF files
            print(pdf_file) # Print file name in log.
            merger.append(os.path.join(path,dir, pdf_file))

    # Write out the merged PDF
    merger.write(path+dir+'.pdf')
    merger.close()
    print(dir + ', Success')




